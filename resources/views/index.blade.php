@extends('layouts.app')

@section('content')	
	<header>
		<nav class="navbar fixed-top navbar-expand-lg" id="scrollOpacity">
			<div class="container">
				<a href="#" class="navbar-brand logo">Arifal</a>
				<div class="">
					<ul class="navbar-nav mr-auto mt-2 mt-lg-0">
						<li class="nav-items mr-4"><a href="">Home</a></li>
						<li class="nav-items mr-4"><a href="">About</a></li>
						<li class="nav-items mr-4"><a href="">Contact</a></li>
						<li class="nav-items mr-4"><a href="">Blog</a></li>
					</ul>
				</div>
			</div>
		</nav>
	</header>
	<section class="page1">
		<div class="container">
			<div class="row">
				<div class="col-6">
					<div class="box">
						<div class="welcome-text">
							<h1>
								Hello
								<br>
								I'am Arifal
							</h1>
							<p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Impedit nisi nobis saepe necessitatibus iste quisquam reiciendis voluptate suscipit minima deleniti deserunt veritatis iusto, amet non dolore minus aliquid aliquam provident?</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="page2">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="menu-experience">
						<h1>My Gallery</h1>
					</div>
				</div>
			</div>
		</div>
		<div class="container-fluid">
			<div class="row image-grid">
				<div class="col-12 col-sm-6 col-lg-3 single_gallery_item image1 mb-30">
					<div class="single-gallery-content">
						<img src="img/ipal.jpeg" alt="">
					</div>
				</div>
				<div class="col-12 col-sm-6 col-lg-3 single_gallery_item image2 mb-30">
					<div class="single-gallery-content">
						<img src="img/IMG_20210427_175840_415.jpg" alt="">
					</div>
				</div>
				<div class="col-12 col-sm-6 col-lg-3 single_gallery_item image3 mb-30">
					<div class="single-gallery-content">
						<img class="img3" src="img/IMG_20210109_204504_284.jpg" alt="">
					</div>
				</div>
				<div class="col-12 col-sm-6 col-lg-3 single_gallery_item image4 mb-30">
					<div class="single-gallery-content">
						<img src="img/arifal.jpeg" alt="">
					</div>
				</div>
				<div class="col-12 col-sm-6 col-lg-3 single_gallery_item image5 mb-30">
					<div class="single-gallery-content">
						<img src="img/arifal.jpeg" alt="">
					</div>
				</div>
				<div class="col-12 col-sm-6 col-lg-3 single_gallery_item image6 mb-30">
					<div class="single-gallery-content">
						<img src="img/arifal.jpeg" alt="">
					</div>
				</div>
				<div class="col-12 col-sm-6 col-lg-3 single_gallery_item image7 mb-30">
					<div class="single-gallery-content">
						<img src="img/arifal.jpeg" alt="">
					</div>
				</div>
				<div class="col-12 col-sm-6 col-lg-3 single_gallery_item image8 mb-30">
					<div class="single-gallery-content">
						<img src="img/ipal.jpeg" alt="">
					</div>
				</div>
				<div class="col-12 col-sm-6 col-lg-6 single_gallery_item image11 mb-30">
					<div class="single-gallery-content">
						<img class="bigimage1" src="img/IMG_20201130_192039_543.jpg" alt="">
					</div>
				</div>
				<div class="col-12 col-sm-6 col-lg-3 single_gallery_item image9 mb-30">
					<div class="single-gallery-content">
						<img class="bigimage2" src="img/IMG_20210529_181434_915.jpg" alt="">
					</div>
				</div>
				<div class="col-12 col-sm-6 col-lg-3 single_gallery_item image10 mb-30">
					<div class="single-gallery-content">
						<img src="img/ipal.jpeg" alt="">
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="page3">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="section-heading text-center">
						<h2>Follow Instagram</h2>
						<p>@miniaturarifal</p>
					</div>
				</div>
			</div>
		</div>
		<div class="instagram-feed-area owl-coursel owl-loaded owl-drag">
			<div class="owl-stage-outer">
				<div class="owl-stage">
					<div class="owl-item">
						<div class="single-instagram-item">
							<img src="img/IMG_20210529_181434_915.jpg" alt="">
							<div class="instagram-hover-content text-center d-flex align-items-center justify-content-center">
								<a href="">
									{{-- <i class="bi bi-instagram" aria-hidden="true"></i>
									<span>@miniaturarifal</span> --}}
								</a>
							</div>
						</div>
					</div>
					<div class="owl-item">
						<div class="single-instagram-item">
							<img src="img/IMG_20210109_204504_284.jpg" alt="">
							<div class="instagram-hover-content text-center d-flex align-items-center justify-content-center">
								<a href="">
									{{-- <i class="bi bi-instagram" aria-hidden="true"></i>
									<span>@miniaturarifal</span> --}}
								</a>
							</div>
						</div>
					</div>
					<div class="owl-item">
						<div class="single-instagram-item">
							<img src="img/IMG_20200923_200403_701.jpg" alt="">
							<div class="instagram-hover-content text-center d-flex align-items-center justify-content-center">
								<a href="">
									{{-- <i class="bi bi-instagram" aria-hidden="true"></i>
									<span>@miniaturarifal</span> --}}
								</a>
							</div>
						</div>
					</div>
					<div class="owl-item">
						<div class="single-instagram-item">
							<img src="img/IMG_20210427_175840_415.jpg" alt="">
							<div class="instagram-hover-content text-center d-flex align-items-center justify-content-center">
								<a href="">
									{{-- <i class="bi bi-instagram" aria-hidden="true"></i>
									<span>@miniaturarifal</span> --}}
								</a>
							</div>
						</div>
					</div>
					<div class="owl-item">
						<div class="single-instagram-item">
							<img src="img/arifal.jpeg" alt="">
							<div class="instagram-hover-content text-center d-flex align-items-center justify-content-center">
								<a href="">
									{{-- <i class="bi bi-instagram" aria-hidden="true"></i>
									<span>@miniaturarifal</span> --}}
								</a>
							</div>
						</div>
					</div>
					<div class="owl-item">
						<div class="single-instagram-item">
							<img src="img/IMG_20191123_143501_254.jpg" alt="">
							<div class="instagram-hover-content text-center d-flex align-items-center justify-content-center">
								<a href="">
									{{-- <i class="bi bi-instagram" aria-hidden="true"></i>
									<span>@miniaturarifal</span> --}}
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<footer>
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="footer-content d-flex align-items-center justify-content-between">
						<div class="copywrite-text">
							<p>
								Copyright ©
								<script>
									document.write(new Date().getFullYear());
								</script>
								I made this with love
								<i class="bi bi-heart"></i>
								By Arifal
							</p>
						</div>
						<div class="footer-logo">
							<p>Arifal</p>
						</div>
						<div class="social-info">
							<a href="">
								<i class="bi bi-facebook" aria-hidden="true"></i>
							</a>
							<a href="">
								<i class="bi bi-instagram" aria-hidden="true"></i>
							</a>
							<a href="">
								<i class="bi bi-linkedin" aria-hidden="true"></i>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
@endsection
@push('scripts')
	<script>
		console.log('test');
	</script>		
@endpush
